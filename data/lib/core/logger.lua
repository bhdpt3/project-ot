Spdlog = {}

Spdlog.info = function(msg)
    print("[INFO] ".. msg)
end

Spdlog.warn = function(msg)
    print("[WARN] ".. msg)
end

Spdlog.error = function(msg)
    print("[ERROR] ".. msg)
end